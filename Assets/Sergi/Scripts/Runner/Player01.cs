﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player01 : MonoBehaviour {
    
     Rigidbody rb;
     Renderer rend;

    public GameObject GO;
    public GameObject Caution;
    
   public bool saltoDeveloped;
   public bool visibleModel;
   public bool optimized;

    public int score;
    float randomSalto;
    bool canJump;
    int scorefinal;
    float cont;
	// Use this for initialization
	void Start () {
        
        scorefinal = PlayerPrefs.GetInt("score", 0);
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();

        if (scorefinal < 5)
        {
            saltoDeveloped = visibleModel = optimized = false;
        }
        else if (scorefinal >= 5 && scorefinal < 10)
        {
            visibleModel = true;
            saltoDeveloped = false;
            optimized = false;
        }
        else if (scorefinal >= 10 && scorefinal < 15)
        {
            visibleModel = true;
            saltoDeveloped = false;
            optimized = true;
        }
        else {
            saltoDeveloped = true;
            visibleModel = true;
            optimized = true;
        }
        cont = 0;
       
	}

    // Update is called once per frame
    void Update()
    {
        if (cont >= 1f)
        {
            score += 10;
            cont = 0;
        }
        else
        {
            cont += Time.deltaTime;
        }
          
        
        if (Input.GetKeyDown("space"))
        {
            if (canJump)
            {
                canJump = false;
                if (saltoDeveloped)
                {
                    rb.AddForce(new Vector3(0.0f, 250f, 1.0f));
                }
                else
                {
                    randomSalto = Random.Range(100, 500);
                    rb.AddForce(new Vector3(0.0f, randomSalto, 1.0f));
                }
            }
        }
        if (!saltoDeveloped || !visibleModel || !optimized)
        {
            if (Input.anyKeyDown)
            {
                //Caution.gameObject.transform.position = 
                Caution.GetComponent<RectTransform>().localPosition = new Vector3(Random.Range(-300, 300), Random.Range(-300, 300), 0);
                Caution.SetActive(true);
            }
        }
    }


    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "floor")
        {
            canJump = true;
        }
        if (col.gameObject.tag == "obstaculo")
        {
            GO.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }
}
