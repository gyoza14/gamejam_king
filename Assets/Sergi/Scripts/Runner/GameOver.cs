﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {




    public Player01 player;
    public Text score;
    public Text plataforma;
    public Text vidaProyecto;
    public Text descargas;
    public Text costeProduccion;
    public Text ganancias;
    public GameObject GameOverUI;
    public GameObject Statics;
    public Sprite[] estadisticas;
    float posX;
    float StposX;
    // Use this for initialization
    void Start () {
        posX = GameOverUI.GetComponent<RectTransform>().localPosition.x;
       StposX = Statics.GetComponent<RectTransform>().localPosition.x;
   
        int randomTexto = Random.Range(0, 3);
        switch (randomTexto)
        {
            case 0:
                plataforma.text = "PC";
                break;
            case 1:
                plataforma.text = "Mobile";
                break;
            case 2:
                plataforma.text = "Console";
                break;
            default:
                break;
        }

        randomTexto = Random.Range(0, 15);
        vidaProyecto.text = randomTexto.ToString() + " Months";

        randomTexto = Random.Range(1, 10000);
        descargas.text = randomTexto.ToString();

        int coste = PlayerPrefs.GetInt("money",0);
        costeProduccion.text = coste.ToString();

        randomTexto = Random.Range(1, 100000);
        ganancias.text = randomTexto.ToString() + " € ";

        if(randomTexto > coste)
        {
            Statics.GetComponent<Image>().sprite = estadisticas[0];
        }
        else
        {
            Statics.GetComponent<Image>().sprite = estadisticas[1];
        }

    }
	
	// Update is called once per frame
	void Update () {
        score.text = player.score.ToString();
       
        if (posX - 1 <= -300)
        {

            GameOverUI.GetComponent<RectTransform>().localPosition = new Vector3(posX, 0, 0);
            Statics.GetComponent<RectTransform>().localPosition = new Vector3(StposX, 0, 0);
        }
        else
        {
            posX -= 5f;  
            StposX -= 10f;
            Statics.GetComponent<RectTransform>().localPosition = new Vector3(StposX, 0, 0);
            GameOverUI.GetComponent<RectTransform>().localPosition = new Vector3(posX, 0, 0);
        }
    }

    public void restartGame()
    {
        SceneManager.LoadScene("RunnerSencillo");
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Main");
    }

}
