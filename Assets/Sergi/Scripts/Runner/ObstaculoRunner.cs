﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaculoRunner : MonoBehaviour {

    Vector3 initpos;
    public float speed=2.5f;
  
	// Use this for initialization
	void Start () {
        initpos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);

	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(-Time.deltaTime*speed, 0f, 0f,Space.World);
	}


    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "respawn")
        {
            transform.position = new Vector3(initpos.x, initpos.y, initpos.z);
        }

    }
}
