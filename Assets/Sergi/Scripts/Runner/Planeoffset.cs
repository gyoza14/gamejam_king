﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Planeoffset : MonoBehaviour {

    public Renderer textSuelo;
    public List<Material> mats = new List<Material>();
     float offset =0.5f;
    public Player01 player;
    int offType; 
    // Use this for initialization
    void Start () {
        textSuelo = GetComponent<Renderer>();
        offType = Random.Range(1, 5);
        StartCoroutine("ChangeOffset");
    }

    IEnumerator ChangeOffset()
    {
        yield return new WaitForSeconds(0.2f);
        StartCoroutine("ChangeOffset");
        offType = Random.Range(1, 5);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (player.optimized)
        {
            offset -= 0.003f;
            textSuelo.material = mats[1];
        }
        else
        {      
            switch (offType)
            {
                case 1:
                    offset -= 0.001f;
                    break;
                case 2:
                    offset -= 0.008f;
                    break;
                case 3:
                    offset -= 0.005f;
                    break;
                case 4:
                    offset -= 0.010f;
                    break;
                case 5:
                    offset -= 0.003f;
                    break;
                default:
                    break;
            }
        }
        if(!player.optimized || !player.saltoDeveloped || !player.visibleModel)
        {
            textSuelo.material = mats[0];
        }
        textSuelo.material.SetTextureOffset("_MainTex", new Vector2(0, offset));
	}
}
