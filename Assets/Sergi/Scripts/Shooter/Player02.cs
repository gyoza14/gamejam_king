﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player02 : MonoBehaviour {

    public GameObject GO;
    public GameObject Caution;
    float score;
    bool frame;
    bool normalidad;

	// Use this for initialization
	void Start () {
        frame = true;
        normalidad = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (!frame || !normalidad )
        {
            if (Input.anyKeyDown)
            {
                //Caution.gameObject.transform.position = 
                Caution.GetComponent<RectTransform>().localPosition = new Vector3(Random.Range(-300, 300), Random.Range(-300, 300), 0);
                Caution.SetActive(true);
            }
        }
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "obstaculo")
        {
            GO.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }
}
