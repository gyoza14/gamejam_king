﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaculoShooter : MonoBehaviour {
    
    float speed;
    Player02 player;
    Vector3 playerPos;
     Camera cam;
    SpawnerEnemi spawn;
    public int id;
	// Use this for initialization
	void Start () {
        speed = 2f;
        player = GameObject.Find("PlayerShooter").GetComponent<Player02>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        spawn = GameObject.Find("EnemySpawner").GetComponent<SpawnerEnemi>();
        playerPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
      
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 vecPlayerEnemy = new Vector3(playerPos.x - transform.position.x, playerPos.y - transform.position.y, playerPos.z - transform.position.z);

        vecPlayerEnemy = vecPlayerEnemy.normalized;
        transform.Translate((Time.deltaTime/ speed) * vecPlayerEnemy.x, 0f, (Time.deltaTime / speed) * vecPlayerEnemy.z, Space.World);

        if (Input.GetMouseButtonDown(0))
        { 
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.tag == "obstaculo"){
                    Destroy(spawn.enemies[hit.collider.gameObject.GetComponent<ObstaculoShooter>().id].gameObject);
                    spawn.enemies.Remove(spawn.enemies[hit.collider.gameObject.GetComponent<ObstaculoShooter>().id]);
                }
            }
        }
        
        
    }
}
