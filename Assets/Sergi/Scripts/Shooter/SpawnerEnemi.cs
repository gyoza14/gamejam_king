﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemi : MonoBehaviour {

    public GameObject enemy;
    float cont;
    float SpawnerRate;
    int i = 0;
    public List<GameObject> enemies = new List<GameObject>();
	// Use this for initialization
	void Start () {
        cont = 0;
        SpawnerRate = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (cont > SpawnerRate)
        {
           GameObject go =  Instantiate(enemy, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
           // go.GetComponent<ObstaculoShooter>().id = i; 
            enemies.Add(go);
            enemies[enemies.Count-1].gameObject.GetComponent<ObstaculoShooter>().id = enemies.Count - 1;
            //i++;
            cont = 0;
        }
        else
        {
            cont += Time.deltaTime;
        }
    }
}
