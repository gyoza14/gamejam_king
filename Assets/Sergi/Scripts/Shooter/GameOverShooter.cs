﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverShooter : MonoBehaviour {
    public Player02 player;
    public Text score;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       // score.text = player.score.ToString();
    }

    public void restartGame()
    {
        SceneManager.LoadScene("RunnerSencillo");
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Main");
    }
}
