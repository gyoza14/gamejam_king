﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementPlayer : MonoBehaviour {

	float MovSpeed;
	bool CanMoveLeft;
	bool CanMoveRight;
	bool CanMoveUp;
	bool CanMoveDown;
	public GameObject Oficinisto;

	public GameObject Puntoinicio;
	public GameObject DireccionPunto;
	public float ShootRate;
	public float NextShootTime;
	public GameObject Bullet;

	public int score;

	bool BadVel;
	int offType;
	public BulletScript BBullet;

	public bool Moving;
	float cont = 0;
	// Use this for initialization
	void Start () {
		int Score = PlayerPrefs.GetInt ("score", 20);
		MovSpeed = 0.15f;
		NextShootTime = Time.time;
		offType = Random.Range(1, 5);
		StartCoroutine("ChangeVel");
		if (Score > 5 && Score < 10) {
			BadVel = true;
			BBullet.DirectionInverse = true;
		} else if (Score > 10 && Score < 15) {
			BadVel = false;
			BBullet.DirectionInverse = true;
		} else {
			BadVel = false;
			BBullet.DirectionInverse = false;
		}
	
	}

	IEnumerator ChangeVel()
	{
		yield return new WaitForSeconds(0.2f);
		StartCoroutine("ChangeVel");
		offType = Random.Range(1, 5);
	}
	// Update is called once per frame
	void Update () {

		if (cont > 1.0f) {
			score += 10;
			cont = 0;
		
		} else {
			cont += Time.deltaTime;
		}

		if (BadVel==false)
		{
			MovSpeed = 0.15f;
		}
		else
		{      
			switch (offType)
			{
			case 1:
				MovSpeed = 0.10f;
				break;
			case 2:
				MovSpeed = 0.20f;
				break;
			case 3:
				MovSpeed = 0.08f;
				break;
			case 4:
				MovSpeed = 0.25f;
				break;
			case 5:
				MovSpeed = 0.05f;
				break;
			default:
				break;
			}
		}
		if (Moving == false) {
			if (CanMoveRight == true && Input.GetKey (KeyCode.D)) {
				transform.Translate (new Vector3 (MovSpeed, 0, 0));
				Oficinisto.transform.rotation = Quaternion.Euler (0, 90, 0);
			}

			if (CanMoveLeft == true && Input.GetKey (KeyCode.A)) {
				transform.Translate (new Vector3 (-MovSpeed, 0, 0));
				Oficinisto.transform.rotation = Quaternion.Euler (0, -90, 0);
			}

			if (CanMoveUp == true && Input.GetKey (KeyCode.W)) {
				transform.Translate (new Vector3 (0, 0, MovSpeed));
				Oficinisto.transform.rotation = Quaternion.Euler (0, 0, 0);
			}

			if (CanMoveDown == true && Input.GetKey (KeyCode.S)) {
				transform.Translate (new Vector3 (0, 0, -MovSpeed));
				Oficinisto.transform.rotation = Quaternion.Euler (0, 180, 0);
			}

			if (this.transform.position.x < -11f) {
				CanMoveLeft = false;
			} else {
				CanMoveLeft = true;
				//Debug.Log ("He salidoL");
			}
			if (this.transform.position.x > 14.3f) {
				CanMoveRight = false;
			} else {
				CanMoveRight = true;
				//Debug.Log ("He salidoL");
			}
			if (this.transform.position.z > -0.1f) {
				CanMoveUp = false;
			} else {
				CanMoveUp = true;
				//Debug.Log ("He salidoL");
			}
			if (this.transform.position.z < -25.13f) {
				CanMoveDown = false;
			} else {
				CanMoveDown = true;
				//Debug.Log ("He salidoL");
			}

			if (Input.GetMouseButtonDown (0)) {
				Shooting ();
			}
		}
	}

	public void Shooting()
	{	
		
		if (Time.time > NextShootTime) 
		{
			Instantiate (Bullet, DireccionPunto.transform.position, DireccionPunto.transform.rotation);
			NextShootTime = Time.time + ShootRate;

			Shooting ();
			//Debug.Log ("Bala Disparada en el momento: "+Time.time);
		}
	}

	void IncreaseScore(){
		
		score += 10;
		Debug.Log ("hola");

	}
	public void GOver(){
		//Cargar Menu GameOver + timescale 0;
		//PauseMenu.SetActive (true);
		Time.timeScale = 0f;
		//GameisPaused = true;
		Cursor.visible = true;
	}


}
