﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletScript : MonoBehaviour {

	float VelBullet;
	public bool DirectionInverse;


	GameObject SceneControllerr;

	// Use this for initialization
	void Start () {
		VelBullet = 0.5f * Time.deltaTime * 50f;
		SceneControllerr = GameObject.Find ("Escena");
	}
	
	// Update is called once per frame
	void Update () {
		if (DirectionInverse == false) {
			transform.Translate (new Vector3 (0, 0, VelBullet));
		} else {
			transform.Translate (new Vector3 (0, 0, -VelBullet));
		}

	}

	void OnTriggerEnter(Collider Other){

		if (Other.tag == "Pared") {
			Destroy (this.gameObject);
			//Debug.Log("SayHello");
			}
		if (Other.tag == "Enemy") {
			Debug.Log ("Pium");
			//SceneControllerr.GetComponent<MovementPlayer>().IncreaseScore();
			Destroy (this.gameObject);
			Destroy (Other.gameObject);
		}
	}
	void OnTriggersStay(Collider Other){

		if (Other.tag == "Enemy") {
			Debug.Log ("Pium");
			//SceneControllerr.GetComponent<MovementPlayer>().IncreaseScore();
			Destroy (this.gameObject);
			Destroy (Other.gameObject);
		}
	}
}
