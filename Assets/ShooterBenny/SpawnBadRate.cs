﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBadRate : MonoBehaviour {

	public GameObject[] BadGuys;
	public GameObject BadGuy;

	float NextSecuenciaTime;
	public float SecuenciaRate = 5f;

	int Randm;


	// Use this for initialization
	void Start () {
		NextSecuenciaTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Time.time > NextSecuenciaTime) {

			Randm = Random.Range (0, BadGuys.Length);
			Debug.Log (Randm);
			Instantiate (BadGuy, BadGuys [Randm].transform.position, Quaternion.identity);


			NextSecuenciaTime = Time.time + SecuenciaRate;
		}
	}
}
