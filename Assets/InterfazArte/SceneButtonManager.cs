﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneButtonManager : MonoBehaviour {

	bool GameisPaused;
	public GameObject PauseMenu;
	public GameObject CreditsMenu;
	public GameObject PpalMenu;
	public GameObject ScoreGO;

	public GameObject Player;

	void Start(){
		
		Player = GameObject.Find ("PlayerPos");
	}
	// Update is called once per frame
	void Update () {
		
	}
	public void ResumeGame(){
	}

	public void GoToScene(string NombreScene){
		SceneManager.LoadScene (NombreScene);
	}

	public void PauseGame(){
		//PauseMenu.SetActive (true);
		//Time.timeScale = 0f;
		GameisPaused = true;
		Cursor.visible = true;
		//Pause.Play ();
	}

	public void Resume(){
		//PauseMenu.SetActive (false);
		//Time.timeScale = 1f;
		GameisPaused = false;
		Cursor.visible = false;
		//Pause.Play ();

	
	}

	public void ShowGO(){
		ScoreGO.SetActive(true);
		Time.timeScale = 0f;
		Player.GetComponent<MovementPlayer>().Moving=true;
			
	}
	public void ShowRestart(){
		ScoreGO.SetActive(false);
		Time.timeScale = 1f;
		Player.GetComponent<MovementPlayer>().Moving=false;

	}


}
