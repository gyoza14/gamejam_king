﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

	GameManager GM;
	public GameObject[] objects;


	GameObject GO;
	public bool remake = true;

	//List<GameObject> GO;

	// Use this for initialization
	void Start () {

		GM = GameObject.Find("GameManager").GetComponent<GameManager>();
		//GO = new List<GameObject> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (!GM.hasPuzzle) {
			remake = true;
		}

		if (GM.shouldInstantiateOBJ) {
			int rand = Random.Range (0, 100) % 2;

			for (int i = 0; i < objects.Length; i++) {
				objects [i].gameObject.SetActive (false);
			}

			objects[rand].gameObject.SetActive (true);

			GM.shouldInstantiateOBJ = false;

		} 


	}
}
