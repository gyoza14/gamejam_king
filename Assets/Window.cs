﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour {

	GameManager GM;

	int maxIn;
	public GameObject[] Windows;

	int totalToUnlock;
	int count = 0;

	// Use this for initialization
	void Start () {
		GM = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		totalToUnlock = Random.Range (5, Windows.Length);
	}
	
	// Update is called once per frame
	void Update () {
		setupScreens ();
	}

	void setupScreens(){

		if (!GM.shouldCreateWord) {
			count = 0;
			totalToUnlock = Random.Range (5, Windows.Length);
			for (int i = 0; i < totalToUnlock; i++) {
				Windows [i].gameObject.SetActive (true);
			}
			GM.shouldCreateWord = false;
		}

	}

	public void DismissWindow(int id){

		Windows [id].gameObject.SetActive (false);
		count++;

		if (count >= totalToUnlock) {
			GM.problemSolved ();

		}



	}

	void restart(){
		if (!GM.hasPuzzle) {

		}
	}

}
