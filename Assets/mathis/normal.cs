﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class normal : MonoBehaviour {

	public bool flipNormal;
	public bool selected ;
	public bool onHover;

	public int id;

	FlipNormals fn;

	ObjectSpawner os;

	void Start(){

		fn = this.transform.parent.GetComponent<FlipNormals> ();
		//fn = transform.parent.GetComponent<FlipNormals> ();
		os = GameObject.Find("Spawner").GetComponent<ObjectSpawner>();

		RemakeNormals ();
	}

	void FixedUpdate(){

		if (os.remake) {
			RemakeNormals ();
			os.remake = false;
		}


		if (flipNormal) {
			fn.count [id] = true;
		} else {
			fn.count [id] = false;
		}



		//int aux;
		if (!fn.onHover) {
			onHover = false;
		}

		if (!fn.selected) {
			selected = false;
		}

		if (fn.flipit && selected) {
			flipNormal = true;
		}

		if (onHover && fn.onHover) {
			this.GetComponent<Renderer> ().material.color = new Color(0, 1, 0, 0.1f); 
		}
		else if (selected && fn.selected) {
			this.GetComponent<Renderer> ().material.color = new Color(0, 1, 0, 1.0f); 
		} 
		else {

			int aux;

			if (flipNormal) {
				aux = 0;
			} else {
				aux = 1;
			}
			this.GetComponent<Renderer> ().material = fn.material [aux];

		}
	
	}


	public void RemakeNormals(){
		int tmp = Random.Range (0, 10) % 2;

		flipNormal = false;
		selected = false;
		onHover = false;

		if (tmp == 0) {
			flipNormal = false;
		} else {
			flipNormal = true;
		}

		os.remake = false;
	}

}
