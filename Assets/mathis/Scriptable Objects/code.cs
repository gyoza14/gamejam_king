﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName= "New Code Snippet", menuName = "Code Snippets")]
public class code : ScriptableObject {

	public string codeSnippet;
	public string[] availableOptions;

	public int correct;

}
