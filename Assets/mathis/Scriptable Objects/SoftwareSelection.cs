﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoftwareSelection : MonoBehaviour {

	GameManager GM;

	public Programs prog;
	public Toggle toggle;
	public Text title;
	public Text price;

	// Use this for initialization
	void Start () {

		GM = GameObject.Find("GameManager").GetComponent<GameManager>();


		title.text = prog.name;
		price.text = prog.price.ToString()+"$";
		prog.selected = toggle.isOn;

	}

	public void addProgram(){

		if (toggle.isOn) {
			//Subtract coins
			GM.money -= prog.price;
		} else {
			//Restore the coins
			GM.money += prog.price;
		}
		GM.moneyText.text = GM.budgetText ();
	}

}
