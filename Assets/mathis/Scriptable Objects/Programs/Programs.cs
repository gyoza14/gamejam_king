﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName= "New Program", menuName = "Program")]
public class Programs : ScriptableObject {

	public string name;
	//public Sprite logo;
	public int price;

	public bool selected;

}
