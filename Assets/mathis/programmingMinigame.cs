﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class programmingMinigame : MonoBehaviour {

	public code[] c;
	private int index;

	public Text code;
	public Text[] options;

	GameManager GM;

	// Use this for initialization
	void Start () {
		GM = GameObject.Find("GameManager").GetComponent<GameManager>();

		index = Random.Range (0, c.Length);

		code.text = c [index].codeSnippet;
		//code.text.Replace ("\n", "\n");

		for (int i = 0; i < options.Length; i++) {
			options [i].text = c [index].availableOptions [i];
		}


	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Option(int op){
		if (op == c [index].correct) {

			GM.problemSolved ();

		} else {
			GM.dismissPuzzle ();
		}
	}


}
