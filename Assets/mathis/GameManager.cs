﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	bool canPlay = false;
	int score = 0;


	public bool hasPuzzle = false;

	private int maxTime = 20;
	private bool isGameOver = false;
	public Text timeRemaining;

	public int money;
	private int minMoney = 10000;
	private int maxMoney = 20000;
	public Text moneyText;
	private int amount = 123;

	public int employees;
	public int maxEmployees = 10;
	private int costEmployee = 858;
	public Text emplyeesText;
	int timeperemployee = 10;

	public Button startButton;
	public GameObject software;

	public GameObject screen;
	public GameObject modellingScreen;
	public GameObject programmingSreen;
	public GameObject word;
	public GameObject em;


	public bool shouldInstantiateOBJ;
	public bool shouldDeleteOBJ;
	public bool shouldCreateWord = false;

	public GameObject finiquito; 

	int sum = 500;
	int sub = 1000;

	// Use this for initialization
	void Start () {

		money = Random.Range (minMoney, maxMoney);
		moneyText.text = "Current budget: " + money.ToString () + "$" ;
		software.gameObject.SetActive (true);
		startButton.gameObject.SetActive (true);

		screen.gameObject.SetActive (false);
		modellingScreen.gameObject.SetActive (false);
		programmingSreen.gameObject.SetActive (false);
		em.gameObject.SetActive (false);
		word.gameObject.SetActive (false);

		finiquito.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (canPlay) {
			if (employees != 0) {
				em.gameObject.SetActive (false);
				generatePuzzle ();
			}

			else if (employees == 0) {
				em.gameObject.SetActive (true);
			}
		}
		if(!canPlay){
			//maxTime += employees * timeperemployee;
			timeRemaining.text = "Release date in " + maxTime.ToString() + " days";
			
		}



	}


	public void startGame(){
		software.gameObject.SetActive (false);
		startButton.gameObject.SetActive (false);
		screen.gameObject.SetActive (true);
		canPlay = true;
		StartCoroutine ("mainTimer");
	}

	//Main timer
	IEnumerator mainTimer(){
		yield return new WaitForSeconds (1);

		if (maxTime > 0) {
			StartCoroutine ("mainTimer");
		} else {

			int tmp = Random.Range (0, 10) % 2;
			PlayerPrefs.SetInt ("money", money);
			if (tmp == 0) {

				SceneManager.LoadScene ("RunnerSencillo");
			} else {
				SceneManager.LoadScene ("ShooterSencillo");

			}
		}
		//print (maxTime);
		timeRemaining.text = "Release date in " + maxTime.ToString() + " days";
		maxTime--;

		if (maxTime % 5 == 0) {
			int tt = Random.Range (0, 10) % 2;

			if (tt == 0) {
				employees--;
				money -= 1000;
				emplyeesText.text = employees.ToString() + " employees";
				StartCoroutine ("popup");
			}
		}

		money -= amount;
		moneyText.text = budgetText ();
	}

	IEnumerator popup(){
		finiquito.gameObject.GetComponent<Animation> ().Play ("New Animation");
		finiquito.gameObject.SetActive (true);
		yield return new WaitForSeconds (2);
		finiquito.gameObject.SetActive (false);
	}

	//GameOver checker
	void GameOver(){
		if (maxTime < 0 || money < 0) {
			StopAllCoroutines ();
			isGameOver = true;
			//StopCoroutine ("mainTimer");


			PlayerPrefs.SetInt ("money", money);
			SceneManager.LoadScene ("MenuPPal");

		}
	}

	public void IncrementEmployees(){
		if (employees < maxEmployees) {
			employees++;
			emplyeesText.text = employees.ToString() + " employees";
			RecalculateBudgetEmployees (true);
			maxTime = maxTime + timeperemployee;

		}
	}

	public void DecrementEmployees(){
		if (employees > 0) {
			employees--;
			emplyeesText.text = employees.ToString() + " employees";
			RecalculateBudgetEmployees (false);
			maxTime = maxTime -  timeperemployee;

		}
	}

	void RecalculateBudgetEmployees(bool increase){
		if (increase) {
			money -= costEmployee;
		} else {
			money += costEmployee;
		}
		moneyText.text = budgetText ();
	}

	public string budgetText(){
		return "Current budget: " + money.ToString () + "$" ;
	}


	void generatePuzzle(){

		if (!hasPuzzle) {

			int type = Random.Range (0, 10) % 3;

			if (type == 0) {
				modellingScreen.gameObject.SetActive (true);
				programmingSreen.gameObject.SetActive (false);
				word.gameObject.SetActive (false);
				shouldInstantiateOBJ = true;
				shouldDeleteOBJ = false;

			} else if (type == 1) {
				modellingScreen.gameObject.SetActive (false);
				programmingSreen.gameObject.SetActive (true);
				word.gameObject.SetActive (false);
				//shouldDeleteOBJ = true;

			} else {
				modellingScreen.gameObject.SetActive (false);
				programmingSreen.gameObject.SetActive (false);
				word.gameObject.SetActive (true);
				shouldCreateWord = true;
			}


			hasPuzzle = true;
		}
	}

	public void problemSolved(){
		hasPuzzle = false;

		//TODO: Sumar puntuacion
		money += sum;
		moneyText.text = budgetText ();
		score++;
		PlayerPrefs.SetInt ("score", score);
		shouldCreateWord = false;

	}

	public void dismissPuzzle(){
		hasPuzzle = false;

		//TODO: Restar puntuacion
		money -= sub;
		moneyText.text = budgetText ();
		score--;
		PlayerPrefs.SetInt ("score", score);
	}




}
