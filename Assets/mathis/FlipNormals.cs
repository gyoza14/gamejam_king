﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlipNormals : MonoBehaviour {

	public Material[] material;
	public bool[] count;

	Ray ray;
	RaycastHit hit;

	public bool selected = false;
	public bool onHover = false;

	public bool flipit = false;

	GameManager GM;


	// Use this for initialization
	void Start () {
		GM = GameObject.Find("GameManager").GetComponent<GameManager>();

	}
	
	// Update is called once per frame
	void Update () {
		caster ();	

		bool allSame = true;

		for (int i = 0; i < count.Length; i++) {
			//Donete
			if (count [i] == false) {
				allSame = false;
			} 

			if (allSame) {
				GM.problemSolved ();
			}
		}

	}

	void caster(){
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Physics.Raycast (ray, out hit)) {
			//print (hit.collider.name);
			//hit.collider.gameObject.GetComponent<Renderer> ().material.color = Color.green;
			onHover = true;
			hit.collider.gameObject.GetComponent<normal> ().onHover = true;

			if (Input.GetMouseButtonDown (0)) {
				hit.collider.gameObject.GetComponent<normal> ().selected = true;
				selected = true;
			} 
		} 
		if (Input.GetMouseButtonDown (0) && hit.collider == null){
			StartCoroutine ("wait");
		}

		if (hit.collider == null) {
			onHover = false;
		}


	}


	IEnumerator wait(){

		yield return new WaitForSeconds (0.2f);
		selected = false;

	}

	public void flip(){
		//print ("Something");

		if (selected) {

			//selected = false;
			flipit = true;
		}
	}



}
