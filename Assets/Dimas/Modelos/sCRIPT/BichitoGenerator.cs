﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BichitoGenerator : MonoBehaviour {

    public Transform[] pos;
    GameObject[] go;
    bool[] isOccupied;
    public GameObject[] bichos;

    GameManager GM;

	// Use this for initialization
	void Start () {



        GM = GameObject.Find("GameManager").GetComponent<GameManager>();

		go = new GameObject[GM.maxEmployees];
		isOccupied = new bool[GM.maxEmployees];

        for (int i = 0; i < pos.Length; i++) {
            isOccupied[i] = false;
        }



	}

	// Update is called once per frame
	void Update () {

		for (int i = 0; i < GM.maxEmployees; i++)
        {


			if (!isOccupied [i] && i < GM.employees) {

				//if (go [i].gameObject != null) {

				//	Destroy (go [i].gameObject);
				//}

				int aux = Random.Range (0, bichos.Length);
				go [i] = Instantiate (bichos [aux].gameObject, pos [i]);

				isOccupied [i] = true;
			} else if (i >= GM.employees){
				if (go [i] != null) {
					Destroy (go [i].gameObject);
				}
			}
        }

    }
}
